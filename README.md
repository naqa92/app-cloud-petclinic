# Workflow CI/CD cloud d'une application Java Spring en micro-services

[Source code](https://github.com/spring-petclinic/spring-petclinic-cloud)

Application web permettant de gérer une clinique vétérinaire (vets, customers & visits) :

- Stack technique : Maven, Gitlab, Docker, Terraform, Kubernetes, Helm, Prometheus, Grafana et Loki.
- Cloud Providers : Azure (dev) et DigitalOcean (prod)
- DNS Externe : Route53 (AWS)
- Database : MySQL (via Helm)
- Tests : Sonarcloud (qualité), Kubescape (sécurité) & K6 (charge)
- Autoscaling : HPA
- TLS : cert-manager

![ui](images/ui.png "ui")

## Infrastructure à déployer

Ressources déployées sur le cluster Kubernetes (1 couleur = 1 namespace) :

![schema1](images/schema1.png "schema1")

- *Note : Le micro-service "config-server" a été remplacé par une Config map, "discovery-server" par le DNS natif de Kubernetes et "admin-server" par Prometheus/Grafana.*

Acheminement du traffic externe jusqu'à l'application :

![schema2](images/schema2.png "schema2")

- *Note : La partie monitoring n'a pas été représentée pour simplifier le schéma.*

## Déploiement de l'infrastructure

Compilation et tests unitaire

```bash
mvn clean compile
mvn test
```

![compile_test](images/compile_test.png "compile_test")

Test qualité

```bash
mvn verify sonar:sonar -Dsonar.projectKey=naqa92_app-cloud-petclinic
```

![quality](images/quality.png "quality")

Packaging

```bash
mvn package -DskipTests=true
```

![packaging](images/packaging.png "packaging")

Build et push des images

```bash
docker login -u "${DOCKER_HUB_USERNAME}" -p "${DOCKER_HUB_PASSWORD}"
docker compose build
docker compose push
```

![registry](images/registry.png "registry")

Création du cluster Kubernetes via terraform

```bash
terraform apply -auto-approve -var "do_api_token=${DO_TOKEN}"
```

![iac-do](images/iac-do.png "iac-do")

Déploiement Kubernetes

- *Note : Avant de procéder au déploiement, il faut mettre à jour la variable masquée "$MYKUBECONFIG" lorsque le cluster kubernetes vient d'être créé.*

```yml
# Création : Namespaces

  - kubectl apply -f ./k8s/init-namespace

# Création : PVC + secret MySQL

  - kubectl apply -f ./k8s/${INITPVCNAME}
  - > 
    kubectl -n spring-petclinic create secret generic password-db-mysql
    --from-literal=mysql-root-password='${DB_ROOT_PWD}'
    --from-literal=mysql-replication-password='${DB_ROOT_PWD}'
    --from-literal=mysql-password='${DB_ROOT_PWD}' || true  

# Déploiement : Helmfile + ClusterIssuer + Services

  - ./helmfile apply -f ./k8s/${HELMFILENAME}
  - kubectl apply -f ./k8s/init-ssl
  - kubectl apply -f ./k8s/init-services

# Déploiement : Application + HPA

  - kubectl apply -f ./k8s/init-deployment
  - kubectl apply -f ./k8s/init-hpa
```

![deploy](images/deploy.png "deploy")
![deploy2](images/deploy2.png "deploy2")

Tests non fonctionnels :

- *Note : Avant de procéder aux tests, il faut ajouter l'adresse IP du load balancer généré par l'ingress controller sur Route53 afin de joindre correctement l'application.*

- Test de sécurité (Kubescape)

```bash
kubescape scan framework DevOpsBest --include-namespaces spring-petclinic --compliance-threshold 80
```

![security](images/security.png "security")

- Test de charge (K6)

```bash
k6 run ./performance-test.js
```

![load](images/load.png "load")

## Monitoring (Prometheus/Grafana/Loki/AM)

Dashboard des micro-services

![dashboard1](images/dashboard1.png "dashboard1")

Dashboard des ressources Kubernetes

![dashboard2](images/dashboard2.png "dashboard2")

- *Note : La partie filesystem est vide car les volumes utilisés sont externes au cluster.*

Dashboard des logs

![dashboard3](images/dashboard3.png "dashboard3")

Test de l'alerting

![alert](images/alert.png "alert")

## To improve

- Build multistage Docker pour Spring boot
- Runner Gitlab auto hébergé (+ de performances)
- Remplacer MySQL par un opérateur PostgreSQL (CNPG)
- Passer en mode pull avec ArgoCD (GitOps)
- Automatiser la création du DNS Record de type "A" sur Route53 via Crossplane
