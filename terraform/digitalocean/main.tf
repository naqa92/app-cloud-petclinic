# Variable masquée provenant de Gitlab
variable "do_api_token" {
  description = "DigitalOcean API Token"
}

provider "digitalocean" {
  token = var.do_api_token
}

data "digitalocean_kubernetes_versions" "prefix" {   # Auto upgrade de la version
  version_prefix = "1.29."
}

resource "digitalocean_kubernetes_cluster" "k8s" {
  name = "naqa-k8s"
  region = "fra1"
  version = data.digitalocean_kubernetes_versions.prefix.latest_version    # Utilisation de la dernière version
  node_pool {
    name = "naqapool"
    size = "s-2vcpu-4gb"
    node_count = 3
  }
}