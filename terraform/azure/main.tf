provider "azurerm" {
  features {}

  client_id       = var.azure_client_id   # appId du role "Contributor"
  client_secret   = var.azure_client_secret   # password du role "Contributor"
  tenant_id       = var.azure_tenant_id   # tenant du role "Contributor"
  subscription_id = var.azure_subscription_id   # ID du compte Azure
}

resource "azurerm_resource_group" "rg" {    # Création d'un Resource Group avec les paramètres requis
  name     = "naqaResourceGroup"
  location = "France Central"
}

resource "azurerm_kubernetes_cluster" "k8s" {
  name                = "naqatest-k8s"
  location            = azurerm_resource_group.rg.location    # Utilisation des paramètres du Resource Group
  resource_group_name = azurerm_resource_group.rg.name        # Utilisation des paramètres du Resource Group
  dns_prefix          = "naqatest-k8s"

  default_node_pool {
    name       = "naqatestpool"
    node_count = "2"
    vm_size    = "Standard_D2s_v3"    # 2 vCPUs / 8 Gio RAM / 16 Gio Stockage
    #enable_node_public_ip = true    # False par défaut, permet d'accéder aux services Nodeport
  }

  identity {
    type = "SystemAssigned"   # Permet à Azure de créer automatiquement les roles/permissions si nécessaire
  }
}