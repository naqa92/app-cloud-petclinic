# Variables masquées provenant de Gitlab
variable "azure_client_id" {
  description = "Azure client ID"
}
variable "azure_client_secret" {
  description = "Azure client Secret"
}
variable "azure_tenant_id" {
  description = "Azure tenant ID"
}
variable "azure_subscription_id" {
  description = "Azure subscription ID"
}